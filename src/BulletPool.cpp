#include "BulletPool.h"


BulletPool::BulletPool(
	const sf::Texture & texture,
	int & targetsHit,
	std::vector<std::unique_ptr<thor::Animator<sf::Sprite, int>>> & animator,
	std::vector<std::unique_ptr<sf::Sprite>> & explosion,
	std::vector<std::unique_ptr<sf::Sound>> & sounds
) :
	m_bullets(),
	m_texture(texture),
	m_animators(animator),
	m_explosions(explosion),
	m_sounds(sounds),
	m_tankTargetsHit(targetsHit)
{
	// The first one is available
	m_firstAvailable = &m_bullets[0];
	
	// Each bullet points to the next
	for (int i = 0; i < POOL_SIZE; i++)
	{
		m_bullets[i].setNext(&m_bullets[i + 1]);
	}

	// The last one links to the begining the list
	m_bullets[POOL_SIZE - 1].setNext(m_firstAvailable);
}



/// <summary>
/// Allow for external code to create a new bullet
/// </summary>
/// <param name="posX"> defines the x value of its position </param>
/// <param name="posY"> defines the y value of its position </param>
/// <param name="speed"> defines the speed of the bullet </param>
/// <param name="velY"> defines the y value of its velocity </param>
/// <param name="angle"> defines its angle in degrees </param>
void BulletPool::create(const sf::Texture & texture,float posX, float posY, float speed, float angle)
{
	// Checks if there is any bullet available,
	// if no bullet is available for use than overwrite 1st bullet
	if (m_firstAvailable == nullptr)
	{
		m_firstAvailable = &m_bullets[0];
	}

	Bullet * newBullet = m_firstAvailable;
	m_firstAvailable = newBullet->getNext();

	newBullet->init(texture, posX, posY, speed, angle);
}

/// <summary>
/// Itterates through the bullet array
/// </summary>
/// <param name="dt"> delta time </param>
void BulletPool::update(double dt)
{
	for (auto & bullet : m_bullets)
	{
		if (bullet.update(dt))
		{
			bullet.setNext(m_firstAvailable);
			m_firstAvailable = &bullet;
		}
	}
}

/// <summary>
/// Itterates through the bullet array,
/// and renders them on the window
/// </summary>
/// <param name="window"> defines the target render window </param>
void BulletPool::render(sf::RenderWindow & window)
{
	for (auto & bullet : m_bullets)
	{
		if (bullet.inUse())
		{
			bullet.render(window);
		}
	}
}

/// <summary>
/// Will run collisions for each bullet that is in use
/// </summary>
/// <param name="targets"> defines the container of targets </param>
void BulletPool::collision(std::vector<std::unique_ptr<Target>> & targets)
{
	for (auto & bullet : m_bullets)
	{
		if (bullet.inUse())
		{
			for (unsigned i = 0u; i < targets.capacity(); i++)
			{
				if (CollisionDetector::collision(bullet.getSprite(), targets[i]->getSprite()))
				{
					bullet.setAlive(false);
					m_tankTargetsHit++;
					targets[i]->destroy();
					unsigned iNext = i + 1u;
					if (iNext < targets.capacity())
					{
						targets[iNext]->changeDuration(targets[i]->getTimeLeft());
						targets[iNext]->appearAt(targets[i]->getTime());
					}
					playExplosionSfx();
					playExplosionAnim(bullet);
				}
			}
		}
	}
}

/// <summary>
/// Will run collisions for each bullet that is in use
/// </summary>
/// <param name="collision"> SFML sprite that we are checking collisions against </param>
void BulletPool::collision(const sf::Sprite & collision)
{
	for (auto & bullet : m_bullets)
	{
		if (bullet.inUse())
		{
			if (CollisionDetector::collision(bullet.getSprite(), collision))
			{
				bullet.setAlive(false);
				
				playExplosionSfx();
				playExplosionAnim(bullet);
			}
		}
	}
}

/// <summary>
/// Will run collisions for each bullet that is in use
/// </summary>
/// <param name="size"> SFML limitations of the screen </param>
void BulletPool::collision(const sf::Vector2u & size)
{
	for (auto & bullet : m_bullets)
	{
		if (bullet.inUse())
		{
			const sf::FloatRect & bulletRect(bullet.getSprite().getGlobalBounds());
			if (
				(bulletRect.left + bulletRect.width < 0.0f || bulletRect.left > static_cast<float>(size.x))
				||
				(bulletRect.top + bulletRect.height < 0.0f || bulletRect.top > static_cast<float>(size.y))
				)
			{
				bullet.setAlive(false);
			}
		}
	}
}

/// <summary>
/// Will play the explosion sound,
/// and dynamically expand the vector of sounds,
/// if no sound is available
/// </summary>
void BulletPool::playExplosionSfx()
{
	unsigned j = 0u;
	for (; j < m_sounds.capacity(); j++)
	{
		if (m_sounds[j]->getStatus() != sf::Sound::Status::Playing)
		{
			m_sounds[j]->play();
			return;
		}
	}
	if (j >= m_sounds.capacity() - 1u)
	{
		std::unique_ptr<sf::Sound> soundPtr(
			new sf::Sound(*m_sounds[0])
		);
		soundPtr->stop();
		m_sounds.push_back(std::move(soundPtr));
	}
}

/// <summary>
/// Will play the explosion animation,
/// and dynamically expand the vector of unique pointer of sounds,
/// if no sound is available
/// </summary>
/// <param name="bullet"> current bullet where explosion is taking place </param>
void BulletPool::playExplosionAnim(Bullet & bullet)
{
	unsigned j = 0u;
	for (; j < m_animators.capacity(); j++)
	{
		if (!m_animators[j]->isPlayingAnimation())
		{
			m_explosions[j]->setPosition(bullet.getSprite().getPosition());
			m_animators[j]->playAnimation(0);
			return;
		}
	}
	if (j >= m_animators.capacity() - 1u)
	{
		std::unique_ptr<sf::Sprite> explosionPtr(
			new sf::Sprite(*m_explosions[0])
		);
		m_explosions.push_back(std::move(explosionPtr));
		std::unique_ptr<thor::Animator<sf::Sprite, int>> animatorPtr(
			new thor::Animator<sf::Sprite, int>(*m_animators[0])
		);
		m_animators.push_back(std::move(animatorPtr));
	}
}
