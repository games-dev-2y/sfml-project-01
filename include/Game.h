#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Thor/Animations.hpp"
#include "Thor/Particles.hpp"
#include <time.h>
#include "LevelLoader.h"
#include "KeyHandler.h"
#include "CollisionDetector.h"
#include "Target.h"
#include "Tank.h"

/// <summary>
/// @brief Main class for the SFML Playground project.
/// @author Rafael Girao
/// SFML Mini-Project
/// </summary>
class Game
{
public:
	Game();	
	void run();
protected:	
	void update(double dt);
	void render();
	void processEvents();	
	void processGameEvents(sf::Event&);

	void initWalls();
	void initExplosionAnim();
	void initTarget();
	void initSound();
	void initMusic();
	
	void load(sf::Texture &, const std::string &);
	void load(sf::Font &, const std::string &);
	void load(sf::SoundBuffer &, const std::string &);

	void computeCollisions();
	void computeTankWallCollision();
	void computeTankBoundsCollision();
	void computeBulletWallCollision();
	void computeBulletTargetCollision();
	
	bool checkTargetsDead();

	// main window
	sf::RenderWindow m_window;

	// main game clock
	sf::Clock m_clock;

	// time tracker for countdown timer
	sf::Time m_time;

	// countdown timer in seconds
	float m_counter;

	// main texture spritesheet
	sf::Texture m_texture;

	// Load the game level data
	LevelData m_level;

	// Level
	sf::Sprite m_level_spr; // background level sprite
	sf::Texture m_level_texture; // background level texture

	// obstacle sprites
	std::vector<sf::Sprite> m_sprites;

	// unique pointer to our tank
	std::unique_ptr<Tank> m_tank;

	// key handling
	KeyHandler m_keyHandler;

	// A containter for the Wall sprites
	std::vector<std::unique_ptr<sf::Sprite>> m_wallSprites;

	// Font file for our textboxes
	sf::Font m_font;

	const unsigned int TEXT_SIZE = 52u; // character size of all text
	const sf::Color TEXT_COLOR = sf::Color::Blue; // color of all text
	// will display countdown timer
	sf::Text m_countdownTimer;

	// will display score of the player
	sf::Text m_scoreText;

	// Will handle our explosion animations in our game
	std::vector<std::unique_ptr<thor::Animator<sf::Sprite, int>>> m_explosionAnimators;
	// Will contain our explosion spritesheet texture
	sf::Texture m_explosionTexture;
	// Will handle our explosion spritesheet
	std::vector<std::unique_ptr<sf::Sprite>> m_explosionSprites;
	// Will contain a frame-by-frame explosion animation
	thor::FrameAnimation m_explosionAnimation;
	// Sound buffer containing explosion sound file
	sf::SoundBuffer m_explosionSoundBuffer;
	// vector container of unique pointers of explosion sounds
	std::vector<std::unique_ptr<sf::Sound>> m_explosionSounds;
	// default capacity of explosion vectors
	const int EXPLOSION_CAPACITY = 5;

	// Texture file containing the target spritesheet
	sf::Texture m_targetTexture;
	// vector container of unique pointers of targets
	std::vector<std::unique_ptr<Target>> m_targets;
	// Will contain a frame-by-frame animation of our targets blinking animation
	thor::FrameAnimation m_targetAnimation;
	// vector container of unique pointers of our animators for all targets
	std::vector<std::unique_ptr<thor::Animator<sf::Sprite, int>>> m_targetAnimators;

	// Sound buffer contain our background sound file
	sf::SoundBuffer m_backgroundSoundBuffer;
	// unique pointer of background sound
	std::unique_ptr<sf::Sound> m_backgroundSound;

	// Game over bool
	bool m_gameOver;
};
