#pragma once

#include <SFML/System/Vector2.hpp>
#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>
#include "yaml-cpp\yaml.h"

struct ObstacleData
{
  std::string m_type; // type of obstacle
  sf::Vector2f m_position; // obstacles position
  double m_rotation; // the orientation of the obstacle in degrees
};

struct SpriteSheetData
{
	std::string m_fileName; // file name/path of our spritesheet texture
};

struct SoundData
{
	std::string m_fileName; // file name/path of our explosion sound file
};

struct MusicData
{
	std::string m_fileName; // file name/path of our background music file
};

struct FontData
{
	std::string m_fileName; // file name/path of our font file
};

struct BackgroundData
{
   std::string m_fileName; // file name/path of our background image file
};

struct ExplosionData
{
	std::string m_filename; // file name/path of our explosion spritesheet animation
};

struct TargetData
{
	std::string m_fileName; // file name/path of our target spritesheet texture
	std::vector<sf::Vector2f> m_positions; // container of our targets positions
};

struct ProjectileData
{
	int m_speed; // speed of all bullets in the game
	int m_damage; // damage value of all bullets in the game (THIS VALUE IS NOT USED)
};

struct TankData
{
	sf::Vector2f m_position; // starting position of our tank
	int m_maxProjectile; // maximum number of projectiles in our 
	double m_reloadTime; // amount of time (in seconds) it takes for our tank to fire a 2nd bullet
	std::string m_scoreFileName; // file name/path of our where score yaml file
	int m_previousScore; // the best previous attained score from our score yaml file
};

/// <summary>
/// Will contain all data related to the current level
/// </summary>
struct LevelData
{
	BackgroundData m_background; // contains our level background related data
	SpriteSheetData m_spriteSheet; // contains our spritesheet related data
	SoundData m_sound; // contains our sound related data
	MusicData m_music; // contains our background music related data
	FontData m_font; // contains our font related data
	ExplosionData m_explosion; // contains our explosion related data
	TargetData m_target; // contains our target related data
	ProjectileData m_projectile; // contains our projectile related data
	TankData m_tank; // contains our tank related data
	std::vector<ObstacleData> m_obstacles; // container for our different obstacle related data
};

class LevelLoader
{
public:

   LevelLoader();

   static bool load(int nr, LevelData& level);
   static bool save(int score); // allows for storing the score into the scores.yaml file
};
