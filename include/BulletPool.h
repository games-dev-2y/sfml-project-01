#pragma once

#include "Thor\Animations.hpp"
#include "SFML\Audio\Sound.hpp"
#include "CollisionDetector.h"
#include <memory>
#include "Target.h"
#include "Bullet.h"

/// <summary>
/// @brief Representation of a pool of bullets
/// @author Rafael Girao
/// Applying the Object Pool design pattern from:
/// http://gameprogrammingpatterns.com/object-pool.html
/// </summary>
class BulletPool
{
public:
	BulletPool(
		const sf::Texture &,
		int &,
		std::vector<std::unique_ptr<thor::Animator<sf::Sprite, int>>> &,
		std::vector<std::unique_ptr<sf::Sprite>> &,
		std::vector<std::unique_ptr<sf::Sound>> &
	);
	void create(const sf::Texture &, float, float, float, float);
	void update(double);
	void render(sf::RenderWindow &);

	void collision(std::vector<std::unique_ptr<Target>> &);
	void collision(const sf::Sprite &);
	void collision(const sf::Vector2u &);

	void playExplosionSfx();
	void playExplosionAnim(Bullet &);

	static const int POOL_SIZE = 40;
private:
	// our pool of bullets
	Bullet m_bullets[POOL_SIZE];
	
	// pointer to next available bullet
	Bullet * m_firstAvailable;
	// Contains reference to bullet texture
	const sf::Texture & m_texture;
	// used for animating a explosion in bullet collisions
	std::vector<std::unique_ptr<thor::Animator<sf::Sprite, int>>> & m_animators;
	// used to control the explosion sprite
	std::vector<std::unique_ptr<sf::Sprite>> & m_explosions;
	// used for playing explosion sound in bullet collisions
	std::vector<std::unique_ptr<sf::Sound>> & m_sounds;
	// reference to tanks target hitting
	int & m_tankTargetsHit;
};

