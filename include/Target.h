#pragma once

#include "SFML\Graphics\Sprite.hpp"
#include "SFML\Graphics\RenderWindow.hpp"
#include "Thor\Animations\Animator.hpp"

/// <summary>
/// @author Rafael Girao
/// Representation of all targets on screen
/// </summary>
class Target
{
public:
	Target(const sf::Vector2f &, thor::Animator<sf::Sprite, int> &, float &, const float &);
	Target(const Target &);
	~Target();

	void update();
	void render(sf::RenderWindow &);

	sf::Sprite & getSprite();
	const bool & getAlive() const;
	float getTimeLeft() const;
	const float & getTime() const;
	
	void destroy();
	void setAlive(const bool &);
	void setTimer(float &);
	void changeDuration(const float &);
	void appearAt(const float &);

private:
	/// <summary>
	/// our targets constant position when dead,
	/// off screen
	/// </summary>
	static const struct Position
	{
		float X;
		float Y;
	}DEAD_POS;

	/// <summary>
	/// all our data is being stored inside this struct
	/// </summary>
	struct Data
	{
		bool m_destroyed; // whether or not our target is was destroyed by a bullet
		bool m_animated; // whether or not to animate our target
		bool m_alive; // whether or not our target is alive
		sf::Vector2f m_position; // targets position
		sf::Sprite m_sprite; // targets sprite
		float m_appearAt; // time at which target appears
		float m_duration; // time the target stays alive

		Data(); // explicit default constructor
		Data(const sf::Vector2f &); // explicit position constructor
		Data(const sf::Vector2f &, const float &); // explicit position and time to appear at constructor
		Data(const Data &); // explicit copy constructor
	} m_data;
	thor::Animator<sf::Sprite, int> & m_animator; // reference to our target animator
	float & m_timer; // reference to our current timer
};

