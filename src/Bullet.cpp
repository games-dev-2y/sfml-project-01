#include "Bullet.h"

/// <summary>
/// static constant degrees to radians float,
/// multiply degrees by this float to convert to radians
/// </summary>
float const Bullet::DEG_TO_RAD = thor::Pi / 180.0f;

/// <summary>
/// Default constructor
/// </summary>
Bullet::Bullet() :
	m_alive(false)
{
}

/// <summary>
/// Changes our bullet into the alive state
/// </summary>
/// <param name="texture"> defines the texture of the bullet </param>
/// <param name="posX"> defines the x-coord of its position </param>
/// <param name="posY"> defines the y-coord of its position </param>
/// <param name="speed"> defines the speed of the bullet </param>
/// <param name="angle"> defines its angle in degrees </param>
void Bullet::init(const sf::Texture & texture, float posX, float posY, float speed, float angle)
{
	m_alive = true;
	m_sprite.setTexture(texture);
	sf::IntRect bulletRect(9, 178, 4, 8);
	m_sprite.setTextureRect(bulletRect);
	m_sprite.setOrigin(bulletRect.width / 2.0f, bulletRect.height / 2.0f);
	m_state.m_live.m_position.x = posX; m_state.m_live.m_position.y = posY;
	m_state.m_live.m_speed = speed;
	m_state.m_live.m_rotation = angle;
}

/// <summary>
/// Main Update logic
/// adds velocity to position
/// </summary>
/// <param name="dt"></param>
/// <returns> returns true if the bullet is dead, else false </returns>
bool Bullet::update(double dt)
{
	if (!m_alive)
	{
		return true;
	}
	else
	{
		m_state.m_live.m_position.x +=
			cosf(m_state.m_live.m_rotation * DEG_TO_RAD) * m_state.m_live.m_speed * static_cast<float>(dt / 1000);
		m_state.m_live.m_position.y +=
			sinf(m_state.m_live.m_rotation * DEG_TO_RAD) * m_state.m_live.m_speed * static_cast<float>(dt / 1000);

		m_sprite.setPosition(m_state.m_live.m_position);
		m_sprite.setRotation(m_state.m_live.m_rotation - 90);

		return false;
	}
}

/// <summary>
/// Main rendering logic
/// draws sprite
/// </summary>
/// <param name="window"> defines the window that the sprite draws to </param>
void Bullet::render(sf::RenderWindow & window)
{
	window.draw(m_sprite);
}

/// <summary>
/// Gets whether the bullet is
/// being used state or in the not used state
/// </summary>
/// <returns> returns true for use, false for not </returns>
bool Bullet::inUse() const
{
	return m_alive;
}

/// <summary>
/// Gets the sprite of the bullet
/// </summary>
/// <returns> returns SFML Sprite </returns>
const sf::Sprite & Bullet::getSprite() const
{
	return m_sprite;
}

void Bullet::setAlive(const bool & alive)
{
	m_alive = alive;
}

/// <summary>
/// Gets the next available Bullet
/// </summary>
/// <returns> returns a pointer to the next available Bullet </returns>
Bullet * Bullet::getNext() const
{
	return m_state.m_next;
}

/// <summary>
/// Sets a pointer to the next available Bullet
/// </summary>
/// <param name="next"> defines a pointer to the next Bullet </param>
void Bullet::setNext(Bullet * next)
{
	m_state.m_next = next;
}
/// <summary>
/// 
/// </summary>
/// <returns></returns>
Bullet::State Bullet::getState() const
{
	return m_state;
}

/// <summary>
/// Default constructor,
/// This constructor is required due to the new ISO C++11 standard
/// in http://www.stroustrup.com/C++11FAQ.html#unions
/// quoted here:
/// "If a union has a member with a user-defined constructor, copy, or destructor
/// then that special function is deleted;
/// that is, it cannot be used for an object of the union type. This is new."
/// </summary>
Bullet::State::State()
{
	memset(this, 0, sizeof(m_live));
}

/// <summary>
/// Copy constructor,
/// this is required due to the new ISO C++11 standard
/// (see Default constructor for more info)
/// </summary>
/// <param name="state"> defines what is to be copied over </param>
Bullet::State::State(const State & state)
{
	if (state.m_next != nullptr)
	{
		m_live = state.m_live;
	}
	else
	{
		m_next = state.m_next;
	}
}

/// <summary>
/// Destructor,
/// this is required due to the new ISO C++11 standard
/// (see Default constructor for more info)
/// </summary>
Bullet::State::~State()
{
}