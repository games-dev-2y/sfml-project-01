# README #

This is Project 01 for Programming, where we program simple tank game, where the tank goes around and fires at targets

### What is this repository for? ###

* Project 01 - Develop a simple tank game
* Version 1.0

### How do I get set up? ###

* clone repository
* ensure `SFML_SDK` environment variable exits
* ensure SFML Version SFML 2.3.2 Visual C++ 14 (2015) - 32-bit is installed
* [SFML-2.3.2-windows-vc14-32-bit.zip](http://www.sfml-dev.org/files/SFML-2.3.2-windows-vc14-32-bit.zip)

### Who do I talk to? ###

* [Rafael Plugge](mailto:rafael.plugge@email.com)