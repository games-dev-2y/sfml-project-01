#include "Tank.h"

double const Tank::DEG_TO_RAD = thor::Pi / 180.0f;

Tank::Tank(
	sf::Texture const & texture,
	KeyHandler const & keyHandler,
	double const & deltaTime,
	TankData const & tankData,
	ProjectileData const & bulletData,
	std::vector<std::unique_ptr<thor::Animator<sf::Sprite, int>>> & animators,
	std::vector<std::unique_ptr<sf::Sprite>> & explosions,
	std::vector<std::unique_ptr<sf::Sound>> & sounds
)
: m_texture(texture),
m_rotatable(true),
m_speed(0.0),
m_rotation(0.0),
m_rotationPrev(0.0),
m_turretRotation(0.0),
m_turretRotationPrev(0.0),
m_reloadTime(tankData.m_reloadTime),
m_targetsDestroyed(0),
m_shotsFired(0),
BULLET_SPEED(bulletData.m_speed),
BULLET_DAMAGE(bulletData.m_damage),
KEY_INPUT(keyHandler),
DELTA_TIME(deltaTime),
RELOAD_TIME(tankData.m_reloadTime),
BRAKES_FASTER(3u),
TURNS_FASTER(5u),
TURRET_SPEED(10u),
TANK_SLOW(3.0),
FASTEST_TURN(8u),
m_bulletPool(texture, m_targetsDestroyed, animators, explosions, sounds)
{
	initSprites(tankData.m_position);
}

/// <summary>
/// tank update logic
/// </summary>
void Tank::update()
{
	if (m_reloadTime >= 0.0)
	{
		// keep decrementing reload time
		m_reloadTime -= (DELTA_TIME / 1000);
	}
	
	// handle any keys pressed in this frame
	handleKeyInput();
	
	// move our tank based on direction we are facing and its speed
	m_tankBase.move(
		static_cast<float>(cos(m_rotation * DEG_TO_RAD) * m_speed * (DELTA_TIME / 1000)),
		static_cast<float>(sin(m_rotation * DEG_TO_RAD) * m_speed * (DELTA_TIME / 1000))
	);
	m_tankBase.setRotation(static_cast<float>(m_rotation)); // keep setting the rotation of our tank
	
	m_turret.setPosition(m_tankBase.getPosition()); // make our turret follow our tank
	m_turret.setRotation(static_cast<float>(m_turretRotation)); // keep setting the rotation of our turret

	// our separate bullet update logic to handle our pool of bullets
	m_bulletPool.update(DELTA_TIME);
}

/// <summary>
/// tank render logic
/// </summary>
/// <param name="window"> target window of our rendering </param>
void Tank::render(sf::RenderWindow & window) 
{
	m_bulletPool.render(window);
	window.draw(m_tankBase);
	window.draw(m_turret);
}

/// <summary>
/// increments our speed with a set top speed
/// </summary>
void Tank::increaseSpeed()
{
	if (m_speed < 100.0)
	{
		m_speed += 0.1;
	}
}

/// <summary>
/// decrements our speed with a set reverse top speed
/// </summary>
void Tank::decreaseSpeed()
{
	if (m_speed > -100.0)
	{
		m_speed -= 0.1;
	}
}

/// <summary>
/// increments the rotation of our tank in a encapsulated area
/// (between 0.0 <= rotationAngle < 360.0)
/// </summary>
void Tank::increaseRotation()
{
	m_rotationPrev = m_rotation;
	m_rotation += 0.1;
	if (m_rotation == 360.0)
	{
		m_rotation = 0.0;
	}
}

/// <summary>
/// decrements the rotation of our tank in a encapsulated area
/// (between 0.0 <= rotationAngle < 360.0)
/// </summary>
void Tank::decreaseRotation()
{
	m_rotationPrev = m_rotation;
	m_rotation -= 0.1;
	if (m_rotation == 0.0)
	{
		m_rotation = 359.0;
	}
}

/// <summary>
/// Where all our use input is handled,
/// using our reference to our Game 's keyhandler
/// </summary>
void Tank::handleKeyInput()
{
	if (KEY_INPUT.isPressed(sf::Keyboard::Key::Z))
	{
		for (unsigned i = 0u; i < TURRET_SPEED; i++)
		{
			decreaseTurretRotation();
		}
	}
	else if (KEY_INPUT.isPressed(sf::Keyboard::Key::X))
	{
		for (unsigned i = 0u; i < TURRET_SPEED; i++)
		{
			increaseTurretRotation();
		}
	}
	if (KEY_INPUT.isPressed(sf::Keyboard::Key::Up))
	{
		// If tank is moving backward,
		// brake 3 times faster than reverse speed
		if (m_speed < -TANK_SLOW)
		{
			for (unsigned i = 0u; i < BRAKES_FASTER; i++)
			{
				increaseSpeed();
			}
		}
		else
		{
			increaseSpeed();
		}
	}
	else if (KEY_INPUT.isPressed(sf::Keyboard::Key::Down))
	{
		// If tank is moving forward,
		// brake 3 times faster than forward speed
		if (m_speed > TANK_SLOW)
		{
			for (unsigned i = 0u; i < BRAKES_FASTER; i++)
			{
				decreaseSpeed();
			}
		}
		else
		{
			decreaseSpeed();
		}
	}
	if (m_rotatable)
	{
		if (KEY_INPUT.isPressed(sf::Keyboard::Key::Left))
		{
			// if tank is not standing still
			if (m_speed != 0.0)
			{
				// turning formula
				// makes tank turn slower when going faster and vice-versa
				unsigned turn = static_cast<unsigned>(TURNS_FASTER * ((1 / m_speed) * 100));

				// sets turn to a maximum value encapsulating the maximum amount the tank can turn
				if (turn > FASTEST_TURN)
				{
					turn = FASTEST_TURN;
				}
				for (unsigned i = 0u; i < turn; i++)
				{
					decreaseRotation();
					decreaseTurretRotation();
				}
			}
			else // tank is standing still
			{
				// turn the maximum turning value using the FASTEST_TURN constant
				for (unsigned i = 0u; i < FASTEST_TURN; i++)
				{
					decreaseRotation();
					decreaseTurretRotation();
				}
			}
		}
		else if (KEY_INPUT.isPressed(sf::Keyboard::Key::Right))
		{
			// if tank is not standing still
			if (m_speed != 0.0)
			{
				// turning formula
				// makes tank turn slower when going faster and vice-versa
				unsigned turn = static_cast<unsigned>(TURNS_FASTER * ((1 / m_speed) * 100));
				
				// sets turn to a maximum value encapsulating the maximum amount the tank can turn
				if (turn > FASTEST_TURN)
				{
					turn = FASTEST_TURN;
				}
				for (unsigned i = 0u; i < turn; i++)
				{
					increaseRotation();
					increaseTurretRotation();
				}
			}
			else // tank is standing still
			{
				// turn the maximum turning value using the FASTEST_TURN constant
				for (unsigned i = 0u; i < FASTEST_TURN; i++)
				{
					increaseRotation();
					increaseTurretRotation();
				}
			}
		}
	}
	if (KEY_INPUT.isPressed(sf::Keyboard::Key::Space))
	{
		// if reload time completed
		if (m_reloadTime < 0.0)
		{
			// fires a bullet in our bullet pool
			m_bulletPool.create(
				m_texture,
				m_tankBase.getPosition().x,
				m_tankBase.getPosition().y,
				static_cast<float>(BULLET_SPEED),
				static_cast<float>(m_turretRotation)
			);
			// reset our reload time back up
			m_reloadTime = RELOAD_TIME;
			// increment amount of shots fired for our score algorithm
			m_shotsFired++;
		}
	}
}

/// <summary>
/// increments the rotation of our tanks turret in a encapsulated area
/// (between 0.0 <= rotationAngle < 360.0)
/// </summary>
void Tank::increaseTurretRotation()
{
	m_turretRotationPrev = m_turretRotation;
	m_turretRotation += 0.1;
	if (m_turretRotation == 360.0)
	{
		m_turretRotation = 0.0;
	}
}

/// <summary>
/// decrements the rotation of our tanks turret in a encapsulated area
/// (between 0.0 <= rotationAngle < 360.0)
/// </summary>
void Tank::decreaseTurretRotation()
{
	m_turretRotationPrev = m_turretRotation;
	m_turretRotation -= 0.1;
	if (m_turretRotation == 0.0)
	{
		m_turretRotation = 359.0;
	}
}

/// <summary>
/// will set the position of our tank
/// </summary>
/// <param name="posX"> new x-coordinate </param>
/// <param name="posY"> new y-coordinate </param>
void Tank::setPosition(float posX, float posY)
{
	m_tankBase.setPosition(posX, posY);
}

/// <summary>
/// gets our tank base sprite, needed mostly for collisions
/// </summary>
sf::Sprite const & Tank::getTankBaseSprite() const
{
	return m_tankBase;
}

/// <summary>
/// Gets the accuracy between shots fired and targets hit
/// </summary>
/// <returns> returns a double as the percentage between shots fired and targets hit </returns>
double const Tank::getAccuracy() const
{
	if (m_shotsFired != 0)
	{
		return (m_targetsDestroyed / static_cast<double>(m_shotsFired)) * 100.0;
	}
	return 0.0;
}

/// <summary>
/// Gets the number of shots fired
/// </summary>
int const & Tank::getShotsFired() const
{
	return m_shotsFired;
}

/// <summary>
/// Gets the number of targets destroyed
/// </summary>
int const & Tank::getTargetsDestroyed() const
{
	return m_targetsDestroyed;
}

/// <summary>
/// Gets the pool of bullets used by the tank
/// </summary>
/// <returns> returns a refernce to the BulletPool member </returns>
BulletPool & Tank::getBulletPool()
{
	return m_bulletPool;
}

/// <summary>
/// Contains response to when tank collides into passed sprite,
/// using normal vector mathematics, the response is to,
/// move the tank away from what its collided and half the speed
/// </summary>
/// <param name="collided"> defines the sprite that the tank collided with </param>
void Tank::hitWall(const sf::Sprite & collided)
{
	sf::Vector2f offset = m_tankBase.getPosition() - collided.getPosition();

	offset.x *= (DELTA_TIME / 1000);
	offset.y *= (DELTA_TIME / 1000);

	m_tankBase.move(offset);

	m_rotatable = false;
	m_speed /= 2.0;
	m_turretRotation = m_turretRotationPrev;
	m_rotation = m_rotationPrev;
	m_tankBase.setRotation(static_cast<float>(m_rotation));
	m_turret.setRotation(static_cast<float>(m_turretRotation));
}

/// <summary>
/// Allows our tank to rotate now thats its not inside a wall anymore
/// </summary>
void Tank::noHitWall()
{
	m_rotatable = true;
}

/// <summary>
/// Initializes our tank sprite and our turret sprite
/// </summary>
/// <param name="pos"> position where our tank will be initialized </param>
void Tank::initSprites(sf::Vector2f const & pos)
{
	// Initialise the tank base
	m_tankBase.setTexture(m_texture);
	sf::IntRect baseRect(2, 43, 79, 43);
	m_tankBase.setTextureRect(baseRect);
	m_tankBase.setOrigin(baseRect.width / 2.0f, baseRect.height / 2.0f);
	m_tankBase.setPosition(pos);

	// Initialise the turret
	m_turret.setTexture(m_texture);
	sf::IntRect turretRect(19, 1, 83, 31);
	m_turret.setTextureRect(turretRect);
	m_turret.setOrigin(turretRect.width / 3.0f, turretRect.height / 2.0f);
	m_turret.setPosition(pos);
}