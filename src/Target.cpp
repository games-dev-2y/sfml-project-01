#include "Target.h"

/// <summary>
/// Initialize our static Position struct to constant values
/// </summary>
const Target::Position Target::DEAD_POS = { -100.0f, -100.0f };

/// <summary>
/// Overloaded constructor,
/// Constructs Target at passed position,
/// with reference to a animator that MUST contain a AnimationType::Target
/// </summary>
/// <param name="position"> defines the position of target </param>
/// <param name="animator"> defined as the animator used in animation the target </param>
/// <param name="timer"> defined as the timer counting down in the game loop </param>
/// <param name="appearAt"> defined as the time at which the target appears </param>
Target::Target(const sf::Vector2f & position, thor::Animator<sf::Sprite, int> & animator, float & timer, const float & appearAt) :
	m_animator(animator),
	m_data(position, appearAt),
	m_timer(timer)
{
}

/// <summary>
/// Copy constructor,
/// Constructs a target out of the passed target
/// </summary>
/// <param name="target"> defines the source of the copy </param>
Target::Target(const Target & target) :
	m_animator(target.m_animator),
	m_data(target.m_data),
	m_timer(target.m_timer)
{
}

/// <summary>
/// Destructor,
/// remove all member variables off the stack
/// </summary>
Target::~Target() {}

/// <summary>
/// Target update logic
/// </summary>
void Target::update()
{
	if (m_data.m_alive)
	{
		m_data.m_sprite.setPosition(m_data.m_position);
		if (m_timer < m_data.m_appearAt - (m_data.m_duration * 0.5f))
		{
			if (!m_data.m_animated)
			{
				m_animator.playAnimation(0, true);
				m_data.m_animated = true;
			}
		}
		if (m_timer < m_data.m_appearAt - m_data.m_duration)
		{
			destroy();
		}
	}
	else
	{
		m_data.m_sprite.setPosition(DEAD_POS.X, DEAD_POS.Y);
		if ( (!m_data.m_destroyed) && (m_timer <= m_data.m_appearAt))
		{
			m_data.m_alive = true;
		}
	}
}

/// <summary>
/// Target draw logic
/// to the target window
/// </summary>
/// <param name="window"> defines the target window </param>
void Target::render(sf::RenderWindow & window)
{
	if (m_data.m_alive)
	{
		window.draw(m_data.m_sprite);
	}
}

/// <summary>
/// Returns a reference to the sprite of the target
/// </summary>
/// <returns> returns editable sprite </returns>
sf::Sprite & Target::getSprite()
{
	return m_data.m_sprite;
}

/// <summary>
/// Returns the target's alive state,
/// true for alive, false for dead
/// </summary>
/// <returns> returns const reference bool </returns>
const bool & Target::getAlive() const
{
	return m_data.m_alive;
}

/// <summary>
/// Gets the time left for the target to dissapear
/// </summary>
/// <returns> returns const float reference of time left </returns>
float Target::getTimeLeft() const
{
	return m_timer - (m_data.m_appearAt - m_data.m_duration);
}

const float & Target::getTime() const
{
	return m_timer;
}

/// <summary>
/// Destroys target stopping the spawning mechanic
/// </summary>
void Target::destroy()
{
	m_data.m_alive = false;
	m_data.m_destroyed = true;
}

/// <summary>
/// Changes the target's alive state
/// </summary>
/// <param name="alive"> defines the state </param>
void Target::setAlive(const bool & alive)
{
	m_data.m_alive = alive;
}

/// <summary>
/// Changes the static timer reference
/// </summary>
/// <param name="counter"> defines the timer that is referenced </param>
void Target::setTimer(float & counter)
{
	Target::m_timer = counter;
}

/// <summary>
/// Changes the duration left for the target
/// </summary>
/// <param name="time"> defines the offset by which to change the duration </param>
void Target::changeDuration(const float & time)
{
	m_data.m_duration += time;
}

/// <summary>
/// Sets the time to appear at
/// </summary>
/// <param name="appearAt"> defines the time to appear at </param>
void Target::appearAt(const float & appearAt)
{
	m_data.m_appearAt = appearAt;
}

/// <summary>
/// Default Data struct constructor
/// </summary>
Target::Data::Data() :
	m_destroyed(false),
	m_animated(false),
	m_alive(false),
	m_position(0.0f, 0.0f),
	m_sprite(),
	m_appearAt(),
	m_duration(5.0f)
{
}

/// <summary>
/// Overloaded Data struct constructor,
/// initialized in a defined position
/// </summary>
/// <param name="position"> defines the new position </param>
Target::Data::Data(const sf::Vector2f & position) :
	m_destroyed(false),
	m_animated(false),
	m_alive(false),
	m_position(position),
	m_sprite(),
	m_appearAt(),
	m_duration(5.0f)
{
}

/// <summary>
/// Overloaded Data struct constructor,
/// intialized in a defined position with a defined appear at time
/// </summary>
/// <param name="position"> defines the new position </param>
/// <param name="appearAt"> defines the new appear at time </param>
Target::Data::Data(const sf::Vector2f & position, const float & appearAt) :
	m_destroyed(false),
	m_animated(false),
	m_alive(false),
	m_position(position),
	m_sprite(),
	m_appearAt(appearAt),
	m_duration(5.0f)
{
}

/// <summary>
/// Copy Data struct constructor
/// </summary>
/// <param name="data"> defines the source of the copy </param>
Target::Data::Data(const Data & data) :
	m_destroyed(data.m_destroyed),
	m_animated(data.m_animated),
	m_alive(data.m_alive),
	m_position(data.m_position),
	m_sprite(data.m_sprite),
	m_appearAt(data.m_appearAt),
	m_duration(data.m_duration)
{
}
