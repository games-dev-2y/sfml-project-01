#pragma once
#include <SFML/Graphics.hpp>
#include <Thor\Math.hpp>
#include "OrientedBoundingBox.h"
#include "LevelLoader.h"
#include "KeyHandler.h"
#include "BulletPool.h"

/// <summary>
/// @brief A simple tank controller.
/// @author Rafael Girao
/// 
/// This class will manage all tank
/// movement, rotations, colisions responses
/// and instantiating the pool of bullets
/// </summary>
class Tank
{
public:	
	Tank(
		sf::Texture const &,
		KeyHandler const &,
		double const &,
		TankData const &,
		ProjectileData const &,
		std::vector<std::unique_ptr<thor::Animator<sf::Sprite, int>>> &,
		std::vector<std::unique_ptr<sf::Sprite>> &,
		std::vector<std::unique_ptr<sf::Sound>> &
	);
	void update();
	void render(sf::RenderWindow & window);
	
	void increaseSpeed();
	void decreaseSpeed();
	void increaseRotation();
	void decreaseRotation();

	void handleKeyInput();

	void increaseTurretRotation();
	void decreaseTurretRotation();

	void setPosition(float, float);

	const sf::Sprite & getTankBaseSprite() const;

	double const getAccuracy() const;
	int const & getShotsFired() const;
	int const & getTargetsDestroyed() const;
	BulletPool & getBulletPool();

	void hitWall(const sf::Sprite &);
	void noHitWall();

	// multiply by this to convert degrees to radians
	double static const DEG_TO_RAD;

private:
	void initSprites(sf::Vector2f const & pos);
	// handles the base sprite of our tank
	sf::Sprite m_tankBase;
	// handles the turret sprite of our tank
	sf::Sprite m_turret;
	// storing a reference to the our tank spritesheet
	sf::Texture const & m_texture;
	// defines whether tank is able to rotate
	bool m_rotatable;
	// controls the speed of the tank
	double m_speed;
	// storing current rotation 
	double m_rotation;
	// storing the rotation of the previous update call
	double m_rotationPrev;
	// storing current turret sprite rotation
	double m_turretRotation;
	// storing the turret sprite of the previous update call
	double m_turretRotationPrev;
	// track how long until the bullet can fire again
	double m_reloadTime;
	// track how many targets destroyed
	int m_targetsDestroyed;
	// track how many shots fired
	int m_shotsFired;

	// defines how fast the bullet moves
	int const & BULLET_SPEED;
	// defines the amount of damage
	int const & BULLET_DAMAGE;
	// reference to our key handler in our game class
	KeyHandler const & KEY_INPUT;
	// defines the amount of time between update calls in miliseconds
	double const & DELTA_TIME;
	// defines the amount of time it takes
	double const & RELOAD_TIME;

	// defines how many times faster
	// the tank brakes
	unsigned const BRAKES_FASTER;

	// defines how many times faster
	// the tank turns when moving slow
	unsigned const TURNS_FASTER;

	// defines how many times faster
	// the turret turns with player input
	unsigned const TURRET_SPEED;

	// defines the level
	// at which the tank is considered slow
	double const TANK_SLOW;

	// defines how fast
	// the tank can turn when standing still
	unsigned const FASTEST_TURN;

	// pool of tank bullets
	BulletPool m_bulletPool;

};
