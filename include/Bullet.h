#pragma once

#include <math.h>
#include "Thor\Math.hpp"
#include "SFML\System\Vector2.hpp"
#include "SFML\Graphics\Sprite.hpp"
#include "SFML\Graphics\RenderWindow.hpp"

/// <summary>
/// @brief Representation of single bullet
/// @author Rafael Girao
/// @description Applying the Object Pool design pattern from:
/// http://gameprogrammingpatterns.com/object-pool.html
/// </summary>
class Bullet
{
public:
	Bullet();
	void init(const sf::Texture &, float, float, float, float);
	bool update(double);
	void render(sf::RenderWindow &);

	bool inUse() const;
	const sf::Sprite & getSprite() const;
	void setAlive(const bool &);

	Bullet * getNext() const;

	void setNext(Bullet *);

private:
	bool m_alive;
	
	static const float DEG_TO_RAD;
	
	// SFML sprite used to draw a single bullet
	sf::Sprite m_sprite;

	// Allocates memory for the single largest element
	// and must be managed
	union State
	{
		// State when its in use
		struct
		{
			sf::Vector2f m_position;
			float m_speed;
			float m_rotation;
		} m_live;

		// State when it's available
		// Using the technique called free list from:
		// https://en.wikipedia.org/wiki/Free_list
		Bullet * m_next;
		
		State(); // explicit constructor
		State(const State &); // explicit constructor
		~State(); // explicit destructor
	} m_state;
public:
	State getState() const;
};

