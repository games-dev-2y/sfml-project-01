#include "LevelLoader.h"

void operator >> (const YAML::Node& obstacleNode, ObstacleData& obstacle)
{
   obstacle.m_type = obstacleNode["type"].as<std::string>();
   obstacle.m_position.x = obstacleNode["position"]["x"].as<float>();
   obstacle.m_position.y = obstacleNode["position"]["y"].as<float>();
   obstacle.m_rotation = obstacleNode["rotation"].as<double>();
}

void operator >> (const YAML::Node& backgroundNode, BackgroundData& background)
{
   background.m_fileName = backgroundNode["file"].as<std::string>();
}

void operator >> (const YAML::Node& spritesheetNode, SpriteSheetData& spritesheet)
{
	spritesheet.m_fileName = spritesheetNode["file"].as<std::string>();
}

void operator >> (const YAML::Node& soundNode, SoundData& sound)
{
	sound.m_fileName = soundNode["file"].as<std::string>();
}

void operator >> (const YAML::Node& musicNode, MusicData& music)
{
	music.m_fileName = musicNode["file"].as<std::string>();
}

void operator >> (const YAML::Node& fontNode, FontData& font)
{
	font.m_fileName = fontNode["file"].as<std::string>();
}

void operator >> (const YAML::Node& tankNode, TankData& tank)
{
	std::string pos("position");
	pos += std::to_string(rand() % 4 + 1); // add a number from 1 to 4 to the string position
	tank.m_position.x = tankNode[pos]["x"].as<float>();
	tank.m_position.y = tankNode[pos]["y"].as<float>();
	int randomOffset = tankNode[pos]["randomOffset"].as<int>();
	// generate number between -(randomOffset) and (randomOffset)
	tank.m_position.x += rand() % (randomOffset * 2) - randomOffset;
	tank.m_position.y += rand() % (randomOffset * 2) - randomOffset;

	tank.m_maxProjectile = tankNode["max_projectiles"].as<int>();
	tank.m_reloadTime = tankNode["reload_time"].as<double>();
	tank.m_scoreFileName = tankNode["score_file"].as<std::string>();
}

void operator >> (const YAML::Node& explosionNode, ExplosionData& explosion)
{
	explosion.m_filename = explosionNode["file"].as<std::string>();
}

void operator >> (const YAML::Node& targetNode, TargetData& target)
{
	target.m_fileName = targetNode["file"].as<std::string>();
	target.m_positions.reserve(10);
	std::string pos("position");
	std::string index("");
	for (unsigned i = 0u; i < target.m_positions.capacity(); i++)
	{
		pos = "position" + std::to_string(i+1);
		target.m_positions.push_back(sf::Vector2f());
		target.m_positions[i].x = targetNode[pos]["x"].as<float>();
		target.m_positions[i].y = targetNode[pos]["y"].as<float>();
		int randomOffset = targetNode[pos]["randomOffset"].as<int>();
		target.m_positions[i].x += rand() % randomOffset - (randomOffset / 2);
		target.m_positions[i].y += rand() % randomOffset - (randomOffset / 2);
	}
}

void operator >> (const YAML::Node& projectileNode, ProjectileData& projectile)
{
	projectile.m_speed = projectileNode["speed"].as<int>();
	projectile.m_damage = projectileNode["damage"].as<int>();
}

void operator >> (const YAML::Node& levelNode, LevelData& level)
{
   levelNode["background"] >> level.m_background;
   levelNode["spritesheet"] >> level.m_spriteSheet;
   levelNode["sound"] >> level.m_sound;
   levelNode["music"] >> level.m_music;
   levelNode["font"] >> level.m_font;
   levelNode["explosion"] >> level.m_explosion;
   levelNode["target"] >> level.m_target;
   levelNode["projectile"] >> level.m_projectile;
   levelNode["tank"] >> level.m_tank;

   const YAML::Node& obstaclesNode = levelNode["obstacles"].as<YAML::Node>();
   for (unsigned i = 0; i < obstaclesNode.size(); ++i)
   {
	  ObstacleData obstacle;
	  obstaclesNode[i] >> obstacle;
	  level.m_obstacles.push_back(obstacle);
   }
}

LevelLoader::LevelLoader()
{
}

bool LevelLoader::load(int nr, LevelData& level)
{
   std::stringstream ss;
   ss << "./resources/levels/level";
   ss << nr;
   ss << ".yaml";

   try
   {
	  YAML::Node baseNode = YAML::LoadFile(ss.str());
	  if (baseNode.IsNull())
	  {
		  std::string message("file: " + ss.str() + " not found");
		  throw std::exception(message.c_str());
	  }
	  baseNode >> level;
   }
   catch(YAML::ParserException& e)
   {
       std::cout << e.what() << "\n";
       return false;
   }
   catch (std::exception& e)
   {
	   std::cout << e.what() << "\n";
	   return false;
   }

   try
   {
	   YAML::Node baseNode = YAML::LoadFile(level.m_tank.m_scoreFileName);
	   if (baseNode.IsNull())
	   {
		   std::string message("file: " + ss.str() + " not found");
		   throw std::exception(message.c_str());
	   }

	   level.m_tank.m_previousScore = baseNode["score"]["previous"].as<int>();
   }
   catch (YAML::ParserException& e)
   {
	   std::cout << e.what() << "\n";
	   return false;
   }
   catch (std::exception& e)
   {
	   std::cout << e.what() << "\n";
	   return false;
   }

   return true;
}

bool LevelLoader::save(int score)
{
	std::stringstream ss;
	ss << "./resources/levels/";
	ss << "scores.yaml";

	try
	{
		YAML::Node baseNode = YAML::LoadFile(ss.str());
		if (baseNode.IsNull())
		{
			std::string message("file: " + ss.str() + " not found");
			throw std::exception(message.c_str());
		}
		baseNode["score"]["previous"] = std::to_string(score);
		std::ofstream fs(ss.str());
		if (!fs.is_open())
		{
			fs.open(ss.str());
		}
		fs << baseNode;
		fs.close();
		
	}
	catch (YAML::ParserException& e)
	{
		std::cout << e.what() << "\n";
		return false;
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << "\n";
		return false;
	}
	return true;
}
