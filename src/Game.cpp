#include "Game.h"

/// <summary>
/// @author RG
/// @date 18 October 2016
/// @version 1.0
/// 
/// </summary>

// Updates per milliseconds
static double const MS_PER_UPDATE = 10.0;

/// <summary>
/// @brief Default constructor.
/// 
/// Create a new window and initialise member objects.
/// </summary>
Game::Game() :
	m_window(sf::VideoMode(1024, 600, 32), "SFML Mini-Project", sf::Style::Default)
{
	// Generate a random seed based on time since
	// 00:00 hours, Jan 1, 1970 UTC
	srand(static_cast<unsigned int>(time(nullptr)));

	// enabling vertical sync
	m_window.setVerticalSyncEnabled(true);

	// Level loading

	int currentLevel = 1;
	if (!LevelLoader::load(currentLevel, m_level))
	{
		std::string s("Error level" + std::to_string(currentLevel) + ".yaml has not loaded");
		throw std::exception(s.c_str());
	}

	// Loading in our main SpriteSheet texture
	load(m_texture, m_level.m_spriteSheet.m_fileName);

	// Loading our background level texture
	load(m_level_texture, m_level.m_background.m_fileName);

	// Loading our font file
	load(m_font, m_level.m_font.m_fileName);

	// Loading our explosion spritesheet texture
	load(m_explosionTexture, m_level.m_explosion.m_filename);

	// Loading our target spritesheet texture
	load(m_targetTexture, m_level.m_target.m_fileName);

	// Loading our explosion sound effect
	load(m_explosionSoundBuffer, m_level.m_sound.m_fileName);

	// Loading our background music
	load(m_backgroundSoundBuffer, m_level.m_music.m_fileName);

	// Use levelData to load font file
	// and set any initialization logic
	m_countdownTimer.setFont(m_font);
	m_countdownTimer.setPosition(
		m_window.getSize().x / 2 - m_countdownTimer.getGlobalBounds().width/2.0f,
		0.0f
	);
	m_countdownTimer.setCharacterSize(TEXT_SIZE);
	m_countdownTimer.setColor(TEXT_COLOR);

	// Use levelData to load font file
	// and set any initialization logic
	m_scoreText.setFont(m_font);
	m_scoreText.setPosition(
		0.0f,
		0.0f
	);
	m_scoreText.setCharacterSize(TEXT_SIZE);
	m_scoreText.setColor(TEXT_COLOR);

	//	Use levelData to load the background image and set its position
	m_level_spr.setTexture(m_level_texture);
	m_level_spr.setPosition(0.0f, 0.0f);

	// initializing unique tank pointer, by invoking the Tank constructor
	m_tank.reset(
		new Tank(
			m_texture,
			m_keyHandler,
			MS_PER_UPDATE,
			m_level.m_tank,
			m_level.m_projectile,
			m_explosionAnimators,
			m_explosionSprites,
			m_explosionSounds
		)
	);
	// Countdown from 61 seconds (first second is spent loading in game)
	m_counter = 61.0f;

	// initialize gameover to false
	m_gameOver = false;

	initWalls();
	initExplosionAnim();
	initTarget();
	initSound();
	initMusic();
}



/// <summary>
/// Main game entry point - runs until user quits.
/// </summary>
void Game::run()
{
	sf::Int32 lag = 0;

	while (m_window.isOpen())
	{
		m_time = m_clock.restart();

		lag += m_time.asMilliseconds();

		m_counter -= m_time.asSeconds();

		processEvents();

		while (lag > MS_PER_UPDATE)
		{
			update(MS_PER_UPDATE);
			lag -= static_cast<sf::Int32>(MS_PER_UPDATE);
		}
		update(MS_PER_UPDATE);

		render();
	}
}



/// <summary>
/// @brief Check for events
/// 
/// Allows window to function and exit. 
/// Events are passed on to the Game::processGameEvents() method
/// </summary>
void Game::processEvents()
{
	sf::Event event;
	while (m_window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			m_window.close();
		}
		processGameEvents(event);
	}
}

/// <summary>
/// @brief Handle all user input.
/// 
/// Detect and handle keyboard input.
/// </summary>
/// <param name="event"> system event </param>
void Game::processGameEvents(sf::Event& event)
{
	switch (event.type)
	{
	case sf::Event::KeyPressed:
		m_keyHandler.updateKey(event.key.code, true);
		break;
	case sf::Event::KeyReleased:
		m_keyHandler.updateKey(event.key.code, false);
		break;
	default:
		break;
	}
}

/// <summary>
/// Method to handle all game updates
/// </summary>
/// <param name="time"> update delta time </param>
void Game::update(double dt)
{
	if (!m_gameOver)
	{
		m_tank->update();

		m_countdownTimer.setString(std::to_string(static_cast<int>(m_counter)));
		m_scoreText.setString(
			"Score: " + std::to_string(m_tank->getTargetsDestroyed() * 10)
		);

		for (auto & target : m_targets)
		{
			target->update();
		}
		if (m_counter < 1.0 || (m_counter < 40.0 && checkTargetsDead()))
		{
			m_countdownTimer.setString("GAME-OVER");
			m_countdownTimer.setPosition(
				m_countdownTimer.getPosition().x - (m_countdownTimer.getLocalBounds().width / 2.0f),
				m_countdownTimer.getPosition().y + ( static_cast<float>(m_window.getSize().y) / 2)
			);
			int score = static_cast<int>((m_tank->getTargetsDestroyed() * 100.0) * (m_tank->getAccuracy() / 100.0));
			m_scoreText.setString(
				"Final Score: " + std::to_string(score) + "\n"
				+ "Best Score: " + std::to_string(m_level.m_tank.m_previousScore) + "\n" + 
				+ "Accuracy: " + std::to_string(static_cast<int>(m_tank->getAccuracy())) + "%"
			);
			if (score > m_level.m_tank.m_previousScore)
			{
				LevelLoader::save(score);
			}
			m_gameOver = true;
		}
	}
	else
	{
		if (m_keyHandler.isPressed(sf::Keyboard::Key::Escape))
		{
			m_window.close();
		}
	}

	for (unsigned i = 0u; i < m_explosionAnimators.capacity(); i++)
	{
		m_explosionAnimators[i]->update(sf::milliseconds(static_cast<sf::Int32>(dt)));
		if (m_explosionAnimators[i]->isPlayingAnimation())
		{
			m_explosionAnimators[i]->animate(*m_explosionSprites[i]);
		}
	}
	for (unsigned i = 0u; i < m_targetAnimators.capacity(); i++)
	{
		m_targetAnimators[i]->update(sf::milliseconds(static_cast<sf::Int32>(dt)));
		if (m_targetAnimators[i]->isPlayingAnimation())
		{
			m_targetAnimators[i]->animate(m_targets[i]->getSprite());
		}
	}

	computeCollisions();
}


/// <summary>
/// @brief draw the window for the game.
/// 
/// </summary>
void Game::render()
{
	m_window.clear();

	m_window.draw(m_level_spr); // draw background

	for (auto & target : m_targets)
	{
		target->render(m_window); // draw each target
	}

	for (auto &sprite : m_wallSprites)
	{
		m_window.draw(*sprite); // draw each wall
	}

	m_tank->render(m_window); // draw tank

	for (unsigned i = 0u; i < m_explosionAnimators.capacity(); i++)
	{
		// checks if a animation is playing
		if (m_explosionAnimators[i]->isPlayingAnimation())
		{
			m_window.draw(*m_explosionSprites[i]); // draw a animation sprite
		}
	}

	m_window.draw(m_countdownTimer); // draw a countdown timer Text
	m_window.draw(m_scoreText); // draw score Text

	m_window.display();
}

/// <summary>
/// @brief instanciate our container of wall sprites
/// 
/// </summary>
void Game::initWalls()
{
	sf::IntRect wallRect(2, 129, 33, 23);;
	// Create the walls
	for (ObstacleData const &obstacle : m_level.m_obstacles)
	{
		std::unique_ptr<sf::Sprite> sprite(new sf::Sprite());
		sprite->setTexture(m_texture);
		sprite->setTextureRect(wallRect);
		sprite->setOrigin(wallRect.width / 2.0f, wallRect.height / 2.0f);
		sprite->setPosition(obstacle.m_position);
		sprite->setRotation(static_cast<float>(obstacle.m_rotation));
		m_wallSprites.push_back(std::move(sprite));
	}
}

/// <summary>
/// Initializes our thor FrameAnimation for our Explosion Animation,
/// itterating through our explosion spritesheet
/// capturing every frame in our animation
/// </summary>
void Game::initExplosionAnim()
{
	if (!m_explosionSprites.empty())
	{
		m_explosionSprites.clear();
	}
	m_explosionSprites.reserve(EXPLOSION_CAPACITY);
	sf::IntRect initRect(0, 0, 1, 1);
	sf::IntRect textRect(0, 0, 100, 100);
	for (unsigned i = 0u; i < m_explosionSprites.capacity(); i++)
	{
		std::unique_ptr<sf::Sprite> explosionSpritePtr(
			new sf::Sprite(m_explosionTexture, initRect)
		);
		explosionSpritePtr->setOrigin(textRect.width / 2.0f, textRect.height / 2.0f);
		
		m_explosionSprites.push_back(std::move(explosionSpritePtr));
	}
	for (int y = 0; y < 700; y += 100)
	{
		for (int x = 0; x < 600; x += 100)
		{
			textRect.left = x; textRect.top = y;
			m_explosionAnimation.addFrame(1.0f, textRect);
		}
	}
	m_explosionAnimators.reserve(EXPLOSION_CAPACITY);
	if (!m_explosionAnimators.empty())
	{
		m_explosionAnimators.clear();
	}
	for (unsigned i = 0u; i < m_explosionAnimators.capacity(); i++)
	{
		std::unique_ptr<thor::Animator<sf::Sprite, int>> explosionAnimator(
			new thor::Animator<sf::Sprite, int>()
		);
		explosionAnimator->addAnimation(0, m_explosionAnimation, sf::seconds(3.0f));
		m_explosionAnimators.push_back(std::move(explosionAnimator));
	}
}

/// <summary>
/// Initializes our vector of unique Target pointers and
/// our thor FrameAnimation for our Target Animation,
/// itterating through our target spritesheet
/// capturing every frame in our animation
/// </summary>
void Game::initTarget()
{
	m_targets.reserve(10);
	m_targetAnimators.reserve(10);
	if (!m_targets.empty())
	{
		m_targets.clear();
	}
	if (!m_targetAnimators.empty())
	{
		m_targetAnimators.clear();
	}

	sf::IntRect textureRect(0, 0, 43, 38);
	textureRect.left = textureRect.width * 3;
	float appearAt = 50.0f;
	for (int x = 0; x < 344; x += 43)
	{
		textureRect.left = x;
		m_targetAnimation.addFrame(1.0f, textureRect);
	}
	for (unsigned i = 0u; i < m_targets.capacity(); i++)
	{
		std::unique_ptr<thor::Animator<sf::Sprite, int>> animatorPtr(
			new thor::Animator<sf::Sprite, int>()
		);
		animatorPtr->addAnimation(0, m_targetAnimation, sf::seconds(2.0f));
		m_targetAnimators.push_back(std::move(animatorPtr));
		std::unique_ptr<Target> targetPtr(
			new Target(
				m_level.m_target.m_positions[i], 
				*(m_targetAnimators[i]), 
				m_counter, 
				(appearAt - (5.0f * i))
			)
		);
		targetPtr->getSprite().setTexture(m_targetTexture);
		targetPtr->getSprite().setTextureRect(textureRect);
		targetPtr->getSprite().setOrigin(textureRect.width / 2.0f, textureRect.height / 2.0f);
		m_targets.push_back(std::move(targetPtr));
	}
}

/// <summary>
/// Initializes our vector of unique Sound pointers
/// </summary>
void Game::initSound()
{
	m_explosionSounds.reserve(EXPLOSION_CAPACITY);
	if (!m_explosionSounds.empty())
	{
		m_explosionSounds.clear();
	}
	for (unsigned i = 0u; i < m_explosionSounds.capacity(); i++)
	{
		std::unique_ptr<sf::Sound> soundPtr(
			new sf::Sound(m_explosionSoundBuffer)
		);
		soundPtr->setLoop(false);
		m_explosionSounds.push_back(std::move(soundPtr));
	}
}

/// <summary>
/// Initializes our unique Sound pointer
/// </summary>
void Game::initMusic()
{
	std::unique_ptr<sf::Sound> musicPtr(
		new sf::Sound(m_backgroundSoundBuffer)
	);
	musicPtr->setLoop(true);
	m_backgroundSound = std::move(musicPtr);
	m_backgroundSound->play();
}

/// <summary>
/// Loads from a file using fileName into
/// the referenced SFML Texture
/// </summary>
/// <param name="texture"> defines the texture to be loaded </param>
/// <param name="fileName"> defines the name of texture file </param>
void Game::load(sf::Texture & texture, const std::string & fileName)
{
	if (!texture.loadFromFile(fileName))
	{
		std::string s("Error " + fileName + " has not loaded");
		throw std::exception(s.c_str());
	}
}

/// <summary>
/// Loads from a file using fileName into
/// the referenced SFML Font
/// </summary>
/// <param name="font"> defines the SFML Font </param>
/// <param name="fileName"> defines the name of the font file </param>
void Game::load(sf::Font & font, const std::string & fileName)
{
	if (!font.loadFromFile(fileName))
	{
		std::string s("Error " + fileName + " has not loaded");
		throw std::exception(s.c_str());
	}
}

/// <summary>
/// Loads from a file using fileName into
/// the referenced SFML SoundBuffer
/// </summary>
/// <param name="sound"> defines the SFML SoundBuffer </param>
/// <param name="fileName"> defines the name of the sound file </param>
void Game::load(sf::SoundBuffer & sound, const std::string & fileName)
{
	if (!sound.loadFromFile(fileName))
	{
		std::string s("Error" + fileName + " has not loaded");
		throw std::exception(s.c_str());
	}
}

/// <summary>
/// Computes all in game collisions
/// </summary>
void Game::computeCollisions()
{
	computeTankWallCollision();
	computeTankBoundsCollision();
	computeBulletWallCollision();
	computeBulletTargetCollision();
}

/// <summary>
/// Compute collisions between the tank and all wall sprites
/// this is done using the Separate Axis Theorem collision checking
/// </summary>
void Game::computeTankWallCollision()
{
	for (auto const & wall : m_wallSprites)
	{
		if (CollisionDetector::collision(m_tank->getTankBaseSprite(), *wall))
		{
			m_tank->hitWall(*wall);
		}
		else
		{
			m_tank->noHitWall();
		}
	}
}

/// <summary>
/// Compute collisions between the tank and the game bounds,
/// this is done using basic rectangle collision
/// </summary>
void Game::computeTankBoundsCollision()
{
	if (m_tank->getTankBaseSprite().getPosition().x < 0.0f)
	{
		m_tank->setPosition(
			0.0f, 
			m_tank->getTankBaseSprite().getPosition().y
		);
	}
	else if (m_tank->getTankBaseSprite().getPosition().x > m_window.getSize().x)
	{
		m_tank->setPosition(
			static_cast<float>(m_window.getSize().x),
			m_tank->getTankBaseSprite().getPosition().y
		);
	}
	if (m_tank->getTankBaseSprite().getPosition().y < 0.0f)
	{
		m_tank->setPosition(
			m_tank->getTankBaseSprite().getPosition().x,
			0.0f
		);
	}
	else if (m_tank->getTankBaseSprite().getPosition().y > m_window.getSize().y)
	{
		m_tank->setPosition(
			m_tank->getTankBaseSprite().getPosition().x,
			static_cast<float>(m_window.getSize().y)
		);
	}
}

/// <summary>
/// Computes collisions between all the bullets & all the wall sprites
/// </summary>
void Game::computeBulletWallCollision()
{
	for (std::unique_ptr<sf::Sprite> const &sprite : m_wallSprites)
	{
		m_tank->getBulletPool().collision(*sprite);
	}
	m_tank->getBulletPool().collision(m_window.getSize());
}

/// <summary>
/// Computes collisions between all bullets and the all the targets
/// </summary>
void Game::computeBulletTargetCollision()
{
	m_tank->getBulletPool().collision(m_targets);
}

/// <summary>
/// Itterates through all targets and
/// checks if they are all dead
/// </summary>
/// <returns> returns true if all targets dead, else false </returns>
bool Game::checkTargetsDead()
{
	bool allDead = false;

	int deadTargets = 0;
	for (auto & target : m_targets)
	{
		if (!target->getAlive())
		{
			deadTargets++;
		}
	}
	if (deadTargets == m_targets.capacity())
	{
		allDead = true;
	}

	return allDead;
}